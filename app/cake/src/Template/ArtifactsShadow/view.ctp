<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsShadow $artifactsShadow
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Shadow') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsShadow->has('artifact') ? $this->Html->link($artifactsShadow->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsShadow->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Collection Location') ?></th>
                    <td><?= h($artifactsShadow->collection_location) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Collection Comments') ?></th>
                    <td><?= h($artifactsShadow->collection_comments) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsShadow->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Cdli Comments') ?></th>
                    <td><?= $this->Text->autoParagraph(h($artifactsShadow->cdli_comments)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Acquisition History') ?></th>
                    <td><?= $this->Text->autoParagraph(h($artifactsShadow->acquisition_history)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>



