<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material[]|\Cake\Collection\CollectionInterface $materials
 */
?>


<h3 class="display-4 pt-3"><?= __('Materials') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
<!--             <th scope="col"><?= $this->Paginator->sort('id') ?></th>
 -->            <th scope="col"><?= $this->Paginator->sort('material') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
<!--             <th scope="col"><?= __('Actions') ?></th>
 -->        </tr>
    </thead>
    <tbody>
        <?php foreach ($materials as $material): ?>
        <tr align="left">
<!--             <td><?= $this->Number->format($material->id) ?></td>
 -->            <td><a href="/materials/<?=h($material->id)?>"><?= h($material->material) ?></a></td>
            <td><?= $material->has('parent_material') ? $this->Html->link($material->parent_material->material, ['controller' => 'Materials', 'action' => 'view', $material->parent_material->id]) : '' ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $material->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $material->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $material->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $material->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

