<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsGenre $artifactsGenre
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Genre') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsGenre->has('artifact') ? $this->Html->link($artifactsGenre->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsGenre->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Genre') ?></th>
                    <td><?= $artifactsGenre->has('genre') ? $this->Html->link($artifactsGenre->genre->id, ['controller' => 'Genres', 'action' => 'view', $artifactsGenre->genre->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsGenre->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Genre Uncertain') ?></th>
                    <td><?= $artifactsGenre->is_genre_uncertain ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Comments') ?></th>
                    <td><?= $this->Text->autoParagraph(h($artifactsGenre->comments)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>



