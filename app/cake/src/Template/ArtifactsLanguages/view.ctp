<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsLanguage $artifactsLanguage
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Language') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsLanguage->has('artifact') ? $this->Html->link($artifactsLanguage->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsLanguage->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Language') ?></th>
                    <td><?= $artifactsLanguage->has('language') ? $this->Html->link($artifactsLanguage->language->id, ['controller' => 'Languages', 'action' => 'view', $artifactsLanguage->language->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsLanguage->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Language Uncertain') ?></th>
                    <td><?= $artifactsLanguage->is_language_uncertain ? __('Yes') : __('No'); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>



