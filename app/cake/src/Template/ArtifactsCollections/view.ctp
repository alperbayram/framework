<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCollection $artifactsCollection
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Collection') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsCollection->has('artifact') ? $this->Html->link($artifactsCollection->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsCollection->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Collection') ?></th>
                    <td><?= $artifactsCollection->has('collection') ? $this->Html->link($artifactsCollection->collection->id, ['controller' => 'Collections', 'action' => 'view', $artifactsCollection->collection->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsCollection->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>



