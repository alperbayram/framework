<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty[]|\Cake\Collection\CollectionInterface $dynasties
 */
?>

<h3 class="display-4 pt-3"><?= __('Dynasties') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead  align="left">
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('polity') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dynasties as $dynasty): ?>
        <tr align="left">
            <td><?= h($dynasty->polity) ?></td>
            <td>
                <a href="/dynasties/<?=h($dynasty->id)?>">
                    <?= h($dynasty->dynasty) ?>
                </a>
            </td>
            <td><?= $this->Number->format($dynasty->sequence) ?></td>
            <td><?php if($dynasty->has('provenience')): ?>
            <a href="/proveniences/<?=h($dynasty->provenience->id)?>"><?= h($dynasty->provenience->provenience) ?></a>
            <?php endif; ?> </td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $dynasty->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $dynasty->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $dynasty->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>


