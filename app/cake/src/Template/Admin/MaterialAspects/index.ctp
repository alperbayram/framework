<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialAspect[]|\Cake\Collection\CollectionInterface $materialAspects
 */
?>

<h3 class="display-4 pt-3"><?= __('Material Aspects') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('material_aspect') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materialAspects as $materialAspect): ?>
        <tr>
            <!-- <td><?= $this->Number->format($materialAspect->id) ?></td> -->
            <td><a href="/materialAspects/<?=h($materialAspect->id)?>"><?= h($materialAspect->material_aspect) ?></a></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',  $materialAspect->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $materialAspect->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $materialAspect->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $materialAspect->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

