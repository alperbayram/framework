<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialAspect $materialAspect
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($materialAspect) ?>
            <legend class="capital-heading"><?= __('Edit Material Aspect') ?></legend>
            <?php
                echo $this->Form->control('material_aspect');
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $materialAspect->id],
                ['class' => 'btn btn-danger'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $materialAspect->id)]
            )
        ?>
        </div>

    </div>

</div>
