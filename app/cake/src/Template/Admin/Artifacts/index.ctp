<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact[]|\Cake\Collection\CollectionInterface $artifacts
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('ark_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('credit_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('primary_publication_comments') ?></th>
            <th scope="col"><?= $this->Paginator->sort('cdli_collation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('composite_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('condition_description') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('date_comments') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dates_referenced') ?></th>
            <th scope="col"><?= $this->Paginator->sort('designation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('electronic_publication') ?></th>
            <th scope="col"><?= $this->Paginator->sort('elevation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('excavation_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('findspot_square') ?></th>
            <th scope="col"><?= $this->Paginator->sort('height') ?></th>
            <th scope="col"><?= $this->Paginator->sort('join_information') ?></th>
            <th scope="col"><?= $this->Paginator->sort('museum_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_preservation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_public') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_atf_public') ?></th>
            <th scope="col"><?= $this->Paginator->sort('are_images_public') ?></th>
            <th scope="col"><?= $this->Paginator->sort('seal_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('stratigraphic_level') ?></th>
            <th scope="col"><?= $this->Paginator->sort('surface_preservation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('thickness') ?></th>
            <th scope="col"><?= $this->Paginator->sort('width') ?></th>
            <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('period_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_provenience_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_period_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_type_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('accounting_period') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_school_text') ?></th>
            <th scope="col"><?= $this->Paginator->sort('written_in') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_object_type_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('archive_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('db_source') ?></th>
            <th scope="col"><?= $this->Paginator->sort('weight') ?></th>
            <th scope="col"><?= $this->Paginator->sort('atf_source') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifacts as $artifact): ?>
        <tr>
            <!-- <td><= $this->Number->format($artifact->id) ?></td> -->
            <td><?= h($artifact->ark_no) ?></td>
            <td><?= $this->Number->format($artifact->credit_id) ?></td>
            <td><?= h($artifact->primary_publication_comments) ?></td>
            <td><?= h($artifact->cdli_collation) ?></td>
            <td><?= h($artifact->composite_no) ?></td>
            <td><?= h($artifact->condition_description) ?></td>
            <td><?= h($artifact->created) ?></td>
            <td><?= h($artifact->date_comments) ?></td>
            <td><?= h($artifact->modified) ?></td>
            <td><?= h($artifact->dates_referenced) ?></td>
            <td><?= h($artifact->designation) ?></td>
            <td><?= h($artifact->electronic_publication) ?></td>
            <td><?= h($artifact->elevation) ?></td>
            <td><?= h($artifact->excavation_no) ?></td>
            <td><?= h($artifact->findspot_square) ?></td>
            <td><?= $this->Number->format($artifact->height) ?></td>
            <td><?= h($artifact->join_information) ?></td>
            <td><?= h($artifact->museum_no) ?></td>
            <td><?= h($artifact->artifact_preservation) ?></td>
            <td><?= h($artifact->is_public) ?></td>
            <td><?= h($artifact->is_atf_public) ?></td>
            <td><?= h($artifact->are_images_public) ?></td>
            <td><?= h($artifact->seal_no) ?></td>
            <td><?= h($artifact->stratigraphic_level) ?></td>
            <td><?= h($artifact->surface_preservation) ?></td>
            <td><?= $this->Number->format($artifact->thickness) ?></td>
            <td><?= $this->Number->format($artifact->width) ?></td>
            <td><?php if($artifact->has('provenience')): ?>
            <a href="/proveniences/<?=h($artifact->provenience->id)?>"><?= h($artifact->provenience->provenience) ?></a>
            <?php endif; ?> </td>
            <td><?php if( $artifact->has('period')): ?>
            <a href="/periods/<?=h($artifact->period->id)?>"><?= h($artifact->period->period) ?></a>
            <?php endif; ?> </td>
            <td><?= h($artifact->is_provenience_uncertain) ?></td>
            <td><?= h($artifact->is_period_uncertain) ?></td>
            <td><?php if( $artifact->has('artifact_type')): ?>
            <a href="/artifact-types/<?=h($artifact->artifact_type->id)?>"><?= h($artifact->artifact_type->artifact_type) ?></a>
            <?php endif; ?> </td>
            <td><?= $this->Number->format($artifact->accounting_period) ?></td>
            <td><?= h($artifact->is_school_text) ?></td>
            <td><?= $this->Number->format($artifact->written_in) ?></td>
            <td><?= $this->Number->format($artifact->is_object_type_uncertain) ?></td>
            <td><?php if( $artifact->has('archive')): ?>
            <a href="/archives/<?=h($artifact->archive->id)?>"><?= h($artifact->archive->archive) ?></a>
            <?php endif; ?> </td>
            <td><?= h($artifact->created_by) ?></td>
            <td><?= h($artifact->db_source) ?></td>
            <td><?= $this->Number->format($artifact->weight) ?></td>
            <td><?= h($artifact->atf_source) ?></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view', $artifact->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $artifact->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $artifact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifact->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

