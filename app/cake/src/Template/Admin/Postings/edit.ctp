<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting $posting
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($posting) ?>
            <legend class="capital-heading"><?= __('Edit Posting') ?></legend>
            <?php
                echo $this->Form->control('posting_type_id', ['options' => $postingTypes, 'empty' => true]);
                echo $this->Form->control('slug');
                echo $this->Form->control('published');
                echo $this->Form->control('title');
                echo $this->Form->control('body');
                echo $this->Form->control('lang');
                echo $this->Form->control('created_by');
                echo $this->Form->control('modified_by');
                echo $this->Form->control('publish_start', ['empty' => true]);
                echo $this->Form->control('publish_end', ['empty' => true]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $posting->id],
                ['class' => 'btn btn-danger'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $posting->id)]
            )
        ?>
        </div>

    </div>

</div>
