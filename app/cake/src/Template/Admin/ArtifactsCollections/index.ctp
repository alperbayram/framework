<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCollection[]|\Cake\Collection\CollectionInterface $artifactsCollections
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts Collections') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('collection_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsCollections as $artifactsCollection): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsCollection->id) ?></td> -->
            <td><?= $artifactsCollection->has('artifact') ? $this->Html->link($artifactsCollection->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsCollection->artifact->id]) : '' ?></td>
            <td><?= $artifactsCollection->has('collection') ? $this->Html->link($artifactsCollection->collection->collection, ['controller' => 'Collections', 'action' => 'view', $artifactsCollection->collection->id]) : '' ?></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',  $artifactsCollection->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactsCollection->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $artifactsCollection->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $artifactsCollection->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

