<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCollection $artifactsCollection
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsCollection) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Collection') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('collection_id', ['options' => $collections, 'empty' => true]);
            ?>
        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactsCollection->id],
                ['class' => 'btn btn-danger',
                'confirm' => __('Are you sure you want to delete # {0}?', $artifactsCollection->id)]
            )
        ?>
        </div>

    </div>

</div>
