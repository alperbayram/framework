<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience[]|\Cake\Collection\CollectionInterface $proveniences
 */
?>

<h3 class="display-4 pt-3"><?= __('Proveniences') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('provenience') ?></th>
            <th scope="col"><?= $this->Paginator->sort('region_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('geo_coordinates') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($proveniences as $provenience): ?>
        <tr align="left">
            <!-- <td><?= $this->Number->format($provenience->id) ?></td> -->
            <td><a href="/proveniences/<?=h($provenience->id)?>"><?= h($provenience->provenience) ?></a></td>
            <td><?php if($provenience->has('region')): ?>
            <a href="/regions/<?=h($provenience->region->id)?>"><?= h($provenience->region->region) ?></a>
            <?php endif; ?> </td>
            <td><?= h($provenience->geo_coordinates) ?></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $provenience->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $provenience->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $provenience->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

