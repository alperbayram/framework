<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author $author
 */
?>
<h1 class="display-3 header-txt text-left">Edit Author Details</h1>

<div class="row justify-content-md-center">
    <div class="col-lg boxed ads">
        <legend class="capital-heading"><?= __('Edit Author') ?></legend>
        Full Author Name: <?php echo $author->author ?><br><br>
        <?= $this->Form->create($author) ?>
            <div class="layout-grid text-left">
                <div>
                    First Name:<?php echo $this->Form->control('first', ['label' => false, 'type' => 'text', 'maxLength' => 149]) ?>
                    Email:<?php echo $this->Form->control('email', ['label' => false, 'type' => 'text', 'maxLength' => 150]) ?>
                    East Asian Order:<?php echo $this->Form->control('east_asian_order', ['label' => false, 'type' => 'checkbox']) ?>
                </div>
                <div>
                    Last Name:<?php echo $this->Form->control('last', ['label' => false, 'type' => 'text', 'maxLength' => 149]) ?>
                    Institution:<?php echo $this->Form->control('institution', ['label' => false, 'type' => 'text', 'maxLength' => 255]) ?>
                    ORCID ID:<?php echo $this->Form->control('orcid_id', ['label' => false, 'type' => 'number', 'maxLength' => 16]) ?>
                </div>
               
            </div>
            <div>
            <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $author->id],
                ['class' => 'btn btn-danger',
                'confirm' => __('Are you sure you want to delete # {0}?', $author->id)]
            )
        ?>
        </div>
    </div>
</div>
