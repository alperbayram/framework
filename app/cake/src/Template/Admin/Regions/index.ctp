<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region[]|\Cake\Collection\CollectionInterface $regions
 */
?>
<h3 class="display-4 pt-3"><?= __('Regions') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('region') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($regions as $region): ?>
        <tr align="left">
            <!-- <td><?= $this->Number->format($region->id) ?></td> -->
            <td><a href="/regions/<?=h($region->id)?>"><?= h($region->region) ?></a></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $region->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $region->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $region->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>


