<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region $region
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($region) ?>
            <legend class="capital-heading"><?= __('Add Region') ?></legend>
            <?php
                echo $this->Form->control('region');
                echo $this->Form->control('geo_coordinates');
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
