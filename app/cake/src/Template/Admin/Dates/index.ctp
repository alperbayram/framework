<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Date[]|\Cake\Collection\CollectionInterface $dates
 */
?>

<h3 class="display-4 pt-3"><?= __('Dates') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('day_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('month_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('month_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('year_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ruler_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('absolute_year') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dates as $date): ?>
        <tr>
            <!-- <td><?= $this->Number->format($date->id) ?></td> -->
            <td><?= h($date->day_no) ?></td>
            <td><?= $date->has('month') ? $this->Html->link($date->month->month_no, ['controller' => 'Months', 'action' => 'view', $date->month->id]) : '' ?></td>
            <td><?= h($date->is_uncertain) ?></td>
            <td><?= h($date->month_no) ?></td>
            <td><?= $date->has('year') ? $this->Html->link($date->year->year_no, ['controller' => 'Years', 'action' => 'view', $date->year->id]) : '' ?></td>
            <td><?php if($date->has('dynasty')): ?>
            <a href="/dynasties/<?=h($date->dynasty->id)?>"><?= h($date->dynasty->dynasty) ?></a>
            <?php endif; ?> </td>
            <td><?php if($date->has('ruler')): ?>
            <a href="/rulers/<?=h($date->ruler->id)?>"><?= h($date->ruler->ruler) ?></a>
            <?php endif; ?> </td>
            <td><?= h($date->absolute_year) ?></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',  $date->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $date->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $date->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $date->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>


