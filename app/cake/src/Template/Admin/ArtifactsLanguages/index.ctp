<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsLanguage[]|\Cake\Collection\CollectionInterface $artifactsLanguages
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts Languages') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('language_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_language_uncertain') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsLanguages as $artifactsLanguage): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsLanguage->id) ?></td> -->
            <td><?= $artifactsLanguage->has('artifact') ? $this->Html->link($artifactsLanguage->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsLanguage->artifact->id]) : '' ?></td>
            <td><?= $artifactsLanguage->has('language') ? $this->Html->link($artifactsLanguage->language->language, ['controller' => 'Languages', 'action' => 'view', $artifactsLanguage->language->id]) : '' ?></td>
            <td><?= h($artifactsLanguage->is_language_uncertain) ?></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',  $artifactsLanguage->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactsLanguage->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $artifactsLanguage->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $artifactsLanguage->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

