<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period[]|\Cake\Collection\CollectionInterface $periods
 */
?>


<h3 class="display-4 pt-3"><?= __('Periods') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <!-- <th scope="col"><= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('period') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($periods as $period): ?>
        <tr align="left">
            <!-- <td><= $this->Number->format($period->id) ?></td> -->
            <td><?= $this->Number->format($period->sequence) ?></td>
            <td><a href="/periods/<?=h($period->id)?>"><?= h($period->period) ?></a></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $period->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $period->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $period->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

