<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsExternalResource $artifactsExternalResource
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsExternalResource) ?>
            <legend class="capital-heading"><?= __('Add Artifacts External Resource') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('external_resource_id', ['options' => $externalResources, 'empty' => true]);
                echo $this->Form->control('external_resource_key');
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>
</div>
