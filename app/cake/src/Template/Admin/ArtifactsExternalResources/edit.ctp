<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsExternalResource $artifactsExternalResource
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsExternalResource) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts External Resource') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('external_resource_id', ['options' => $externalResources, 'empty' => true]);
                echo $this->Form->control('external_resource_key');
            ?>
<div>
<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactsExternalResource->id],
                ['class' => 'btn btn-danger',
                'confirm' => __('Are you sure you want to delete # {0}?', $artifactsExternalResource->id)]
            )
        ?>
        </div>
    </div>

</div>
