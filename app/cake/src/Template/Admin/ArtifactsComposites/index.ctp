<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsComposite[]|\Cake\Collection\CollectionInterface $artifactsComposites
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts Composites') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('composite_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsComposites as $artifactsComposite): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsComposite->id) ?></td> -->
            <td><?= h($artifactsComposite->composite_no) ?></td>
            <td><?= $artifactsComposite->has('artifact') ? $this->Html->link($artifactsComposite->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsComposite->artifact->id]) : '' ?></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',  $artifactsComposite->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactsComposite->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $artifactsComposite->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $artifactsComposite->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>
