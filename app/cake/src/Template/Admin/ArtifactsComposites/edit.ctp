<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsComposite $artifactsComposite
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsComposite) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Composite') ?></legend>
            <?php
                echo $this->Form->control('composite_no');
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
            ?>
        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactsComposite->id],
                ['class' => 'btn btn-danger',
                'confirm' => __('Are you sure you want to delete # {0}?', $artifactsComposite->id)]
            )
        ?>
        </div>

    </div>

</div>
