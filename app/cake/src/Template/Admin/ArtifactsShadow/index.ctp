<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsShadow[]|\Cake\Collection\CollectionInterface $artifactsShadow
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts Shadow') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('collection_location') ?></th>
            <th scope="col"><?= $this->Paginator->sort('collection_comments') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsShadow as $artifactsShadow): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsShadow->id) ?></td> -->
            <td><?= $artifactsShadow->has('artifact') ? $this->Html->link($artifactsShadow->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsShadow->artifact->id]) : '' ?></td>
            <td><?= h($artifactsShadow->collection_location) ?></td>
            <td><?= h($artifactsShadow->collection_comments) ?></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',   $artifactsShadow->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactsShadow->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',   $artifactsShadow->id], ['confirm' => __('Are you sure you want to delete # {0}?',   $artifactsShadow->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
               
               
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

