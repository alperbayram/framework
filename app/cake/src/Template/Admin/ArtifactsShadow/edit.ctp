<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsShadow $artifactsShadow
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsShadow) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Shadow') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('cdli_comments');
                echo $this->Form->control('collection_location');
                echo $this->Form->control('collection_comments');
                echo $this->Form->control('acquisition_history');
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
