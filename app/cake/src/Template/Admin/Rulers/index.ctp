<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ruler[]|\Cake\Collection\CollectionInterface $rulers
 */
?>

<h3 class="display-4 pt-3"><?= __('Rulers') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ruler') ?></th>
            <th scope="col"><?= $this->Paginator->sort('period_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($rulers as $ruler): ?>
        <tr align="left">
            <td><?= $this->Number->format($ruler->sequence) ?></td>
            <td><a href="/rulers/<?=h($ruler->id)?>"><?= h($ruler->ruler) ?></a></td>
            <td><?php if($ruler->has('period')): ?>
            <a href="/periods/<?=h($ruler->period->id)?>"><?= h($ruler->period->period) ?></a>
            <?php endif; ?> </td>
            <td><?php if($ruler->has('dynasty')): ?>
            <a href="/dynasties/<?=h($ruler->dynasty->id)?>"><?= h($ruler->dynasty->dynasty) ?></a>
            <?php endif; ?> </td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $ruler->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $ruler->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $ruler->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>


