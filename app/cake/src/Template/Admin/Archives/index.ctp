<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Archive[]|\Cake\Collection\CollectionInterface $archives
 */
?>

<h3 class="display-4 pt-3"><?= __('Archives') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('archive') ?></th>
            <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($archives as $archive): ?>
        <tr align="left">
            <!-- <td><?= $this->Number->format($archive->id) ?></td> -->
            <td><a href="/archives/<?=h($archive->id)?>"><?= h($archive->archive) ?><?= h($archive->archive) ?></a></td>
            <td><?php if($archive->has('provenience')): ?>
            <a href="/proveniences/<?=h($archive->provenience->provenience)?>"><?= h($archive->provenience->id) ?></a>
            <?php endif; ?> </td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $archive->id],['class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $archive->id], ['confirm' => __('Are you sure you want to delete # {0}?', $archive->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

