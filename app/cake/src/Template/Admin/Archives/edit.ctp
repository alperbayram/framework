<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Archive $archive
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($archive) ?>
            <legend class="capital-heading"><?= __('Edit Archive') ?></legend>
            <?php
                echo $this->Form->control('archive');
                echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true]);
            ?>
        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $archive->id],
                ['class' => 'btn btn-danger',
                'confirm' => __('Are you sure you want to delete # {0}?', $archive->id)]
            )
        ?>
        </div>

    </div>


</div>
