<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsSeal $artifactsSeal
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Artifacts Seal') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Seal No') ?></th>
                    <td><?= h($artifactsSeal->seal_no) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsSeal->has('artifact') ? $this->Html->link($artifactsSeal->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsSeal->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsSeal->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>



