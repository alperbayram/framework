<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsSeal[]|\Cake\Collection\CollectionInterface $artifactsSeals
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts Seals') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('seal_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsSeals as $artifactsSeal): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsSeal->id) ?></td> -->
            <td><?= h($artifactsSeal->seal_no) ?></td>
            <td><?= $artifactsSeal->has('artifact') ? $this->Html->link($artifactsSeal->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsSeal->artifact->id]) : '' ?></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',  $artifactsSeal->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactsSeal->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $artifactsSeal->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $artifactsSeal->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
               
               
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

