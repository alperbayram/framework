<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsSeal $artifactsSeal
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsSeal) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Seal') ?></legend>
            <?php
                echo $this->Form->control('seal_no');
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
