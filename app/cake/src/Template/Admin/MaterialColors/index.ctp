<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialColor[]|\Cake\Collection\CollectionInterface $materialColors
 */
?>

<h3 class="display-4 pt-3"><?= __('Material Colors') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('material_color') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materialColors as $materialColor): ?>
        <tr>
            <!-- <td><?= $this->Number->format($materialColor->id) ?></td> -->
            <td><a href="/materialColors/<?=h($materialColor->id)?>"><?= h($materialColor->material_color) ?></a></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $materialColor->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $materialColor->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $materialColor->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

