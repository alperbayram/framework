<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialColor $materialColor
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($materialColor) ?>
            <legend class="capital-heading"><?= __('Edit Material Color') ?></legend>
            <?php
                echo $this->Form->control('material_color');
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $materialColor->id],
                ['class' => 'btn btn-danger'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $materialColor->id)]
            )
        ?>
        </div>

    </div>

</div>
