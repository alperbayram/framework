<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Language[]|\Cake\Collection\CollectionInterface $languages
 */
?>

<h3 class="display-4 pt-3"><?= __('Languages') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('language') ?></th>
            <th scope="col"><?= $this->Paginator->sort('protocol_code') ?></th>
            <th scope="col"><?= $this->Paginator->sort('inline_code') ?></th>
            <th scope="col"><?= $this->Paginator->sort('notes') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($languages as $language): ?>
        <tr align="left">
            <td>
                <?= $this->Number->format($language->sequence) ?>
            </td>
            <td><?php if($language->has('parent_language')): ?>
            <a href="/languages/<?=h($language->parent_language->id)?>"><?= h($language->parent_language->language) ?></a>
            <?php endif; ?> </td>
            <td>
                <a href="/languages/<?=h($language->id)?>">
                    <?= h($language->language) ?>
                </a>
            </td>
            <td><?= h($language->protocol_code) ?></td>
            <td><?= h($language->inline_code) ?></td>
            <td><?= h($language->notes) ?></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $language->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $language->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $language->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

