<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Language $language
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($language) ?>
            <legend class="capital-heading"><?= __('Edit Language') ?></legend>
            <?php
                echo $this->Form->control('sequence');
                echo $this->Form->control('parent_id', ['options' => $parentLanguages, 'empty' => true]);
                echo $this->Form->control('language');
                echo $this->Form->control('protocol_code');
                echo $this->Form->control('inline_code');
                echo $this->Form->control('notes');
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $language->id],
                ['class' => 'btn btn-danger'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $language->id)]
            )
        ?>
        </div>          

    </div>

</div>
