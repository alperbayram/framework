<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading $signReading
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="signReadings view content">
            <h3 class="capital-heading"><?= h($signReading->id) ?></h3>
            <table  cellpadding="0" cellspacing="0" class="table-bootstrap">
                <tr>
                    <th><?= __('Sign Name') ?></th>
                    <td><?= h($signReading->sign_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Sign Reading') ?></th>
                    <td><?= h($signReading->sign_reading) ?></td>
                </tr>
                <tr>
                    <th><?= __('Meaning') ?></th>
                    <td><?= h($signReading->meaning) ?></td>
                </tr>
                <tr>
                    <th><?= __('Period') ?></th>
                    <td><?= $signReading->has('period') ? $this->Html->link($signReading->period->id, ['controller' => 'Periods', 'action' => 'view', $signReading->period->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Provenience') ?></th>
                    <td><?= $signReading->has('provenience') ? $this->Html->link($signReading->provenience->id, ['controller' => 'Proveniences', 'action' => 'view', $signReading->provenience->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Language') ?></th>
                    <td><?= $signReading->has('language') ? $this->Html->link($signReading->language->id, ['controller' => 'Languages', 'action' => 'view', $signReading->language->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Sign Reading Id') ?></th>
                    <td><?= $this->Number->format($signReading->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Preferred Reading') ?></th>
                    <td><?= $signReading->preferred_reading ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="boxed mx-0">
    <?php if (empty($signReading->signReadingsComments)): ?>
        <div class="capital-heading"><?= __('No Related Comments') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Comments') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Author') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </thead>
            <tbody>
                <?php foreach ($signReading->signReadingsComments as $comments): ?>
                <tr>
                    <td><?= h($authors->id) ?></td>
                    <td><?= h($comments->comment) ?></td>
                    <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Authors', 'action' => 'view', $comments->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Authors', 'action' => 'edit', $comments->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Authors', 'action' => 'delete', $comments->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $authors->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
