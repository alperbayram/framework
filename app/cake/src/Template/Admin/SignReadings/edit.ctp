<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading $signReading
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg boxed">
        <div class="signReadings form content">
            <?= $this->Form->create($signReading) ?>
            <fieldset>
                <legend><?= __('Edit Sign Reading') ?></legend>
                <?php
                    echo $this->Form->control('sign_name');
                    echo $this->Form->control('sign_reading');
                    echo $this->Form->control('preferred_reading');
                    echo $this->Form->control('period_id', ['options' => $periods, 'empty' => true]);
                    echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true]);
                    echo $this->Form->control('language_id', ['options' => $languages, 'empty' => true]);
                    echo $this->Form->control('meaning');
                ?>
            </fieldset>
            <div>
            <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
            <?= $this->Form->end() ?>
            <?= $this->Form->postLink(
                __('Delete Sign Reading'),
                ['action' => 'delete', $signReading->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $signReading->id), 'class' => 'btn btn-danger']
            ) ?>
            </div>
        </div>
    </div>
</div>
