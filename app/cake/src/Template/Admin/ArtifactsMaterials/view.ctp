<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsMaterial $artifactsMaterial
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Artifacts Material') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsMaterial->has('artifact') ? $this->Html->link($artifactsMaterial->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsMaterial->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Material') ?></th>
                    <td><?= $artifactsMaterial->has('material') ? $this->Html->link($artifactsMaterial->material->id, ['controller' => 'Materials', 'action' => 'view', $artifactsMaterial->material->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Material Color') ?></th>
                    <td><?= $artifactsMaterial->has('material_color') ? $this->Html->link($artifactsMaterial->material_color->id, ['controller' => 'MaterialColors', 'action' => 'view', $artifactsMaterial->material_color->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Material Aspect') ?></th>
                    <td><?= $artifactsMaterial->has('material_aspect') ? $this->Html->link($artifactsMaterial->material_aspect->id, ['controller' => 'MaterialAspects', 'action' => 'view', $artifactsMaterial->material_aspect->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsMaterial->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Material Uncertain') ?></th>
                    <td><?= $artifactsMaterial->is_material_uncertain ? __('Yes') : __('No'); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>



