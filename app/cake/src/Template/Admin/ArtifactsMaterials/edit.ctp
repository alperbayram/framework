<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsMaterial $artifactsMaterial
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsMaterial) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Material') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts]);
                echo $this->Form->control('material_id', ['options' => $materials]);
                echo $this->Form->control('is_material_uncertain');
                echo $this->Form->control('material_color_id', ['options' => $materialColors, 'empty' => true]);
                echo $this->Form->control('material_aspect_id', ['options' => $materialAspects, 'empty' => true]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
