<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AuthorsPublication[]|\Cake\Collection\CollectionInterface $authorsPublications
 */
?>


<h3 class="display-4 pt-3"><?= __('Author-Publication Links') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('author_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publication_reference') ?></th>
            <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authorsPublications as $authorsPublication): ?>
        <tr>
            <!-- <td><?= $this->Number->format($authorsPublication->id) ?></td> -->
            <td><?= $this->Html->link($authorsPublication->author->author, ['controller' => 'Authors', 'action' => 'view', $authorsPublication->author_id]) ?></td>
            <td><?= $this->Html->link($authorsPublication->publication_id, ['controller' => 'Publications', 'action' => 'view', $authorsPublication->publication_id]) ?></td>
            <td> To be added </td>
            <td><?= h($authorsPublication->sequence) ?></td>
            <td>
            
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $authorsPublication->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $authorsPublication->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $authorsPublication->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>


