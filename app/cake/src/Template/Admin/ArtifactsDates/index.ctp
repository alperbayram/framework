<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDate[]|\Cake\Collection\CollectionInterface $artifactsDates
 */
?>

<h3 class="display-4 pt-3"><?= __('Artifacts Dates') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('date_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsDates as $artifactsDate): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsDate->id) ?></td> -->
            <td><?= $artifactsDate->has('artifact') ? $this->Html->link($artifactsDate->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsDate->artifact->id]) : '' ?></td>
            <td ><a href="/dates/<?=h($artifactsDate->date->id)?>"><?= h($artifactsDate->date->day_no)?></a></td> 
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',  $artifactsDate->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactsDate->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $artifactsDate->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $artifactsDate->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

