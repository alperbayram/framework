<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDate $artifactsDate
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsDate) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Date') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('date_id', ['options' => $dates, 'empty' => true]);
            ?>
<div>
<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactsDate->id],
                ['class' => 'btn btn-danger',
                'confirm' => __('Are you sure you want to delete # {0}?', $artifactsDate->id)]
            )
        ?>
        </div>

    </div>

</div>
