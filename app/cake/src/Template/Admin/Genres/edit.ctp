<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Genre $genre
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($genre) ?>
            <legend class="capital-heading"><?= __('Edit Genre') ?></legend>
            <?php
                echo $this->Form->control('genre');
                echo $this->Form->control('parent_id', ['options' => $parentGenres, 'empty' => true]);
                echo $this->Form->control('genre_comments');
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $genre->id],
                ['class' => 'btn btn-danger'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $genre->id)]
            )
        ?>
        </div>


    </div>

</div>
