<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Genre[]|\Cake\Collection\CollectionInterface $genres
 */
?>

<h3 class="display-4 pt-3"><?= __('Genres') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('genre') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($genres as $genre): ?>
        <tr align="left">
            <!-- <td><= $this->Number->format($genre->id) ?></td> -->
            <td><a href="/genres/<?=h($genre->id)?>"><?= h($genre->genre) ?></a></td>
            <td><?php if($genre->has('parent_genre')): ?>
            <a href="/genres/<?=h($genre->parent_genre->id)?>"><?= h($genre->parent_genre->genre) ?></a>
            <?php endif; ?> </td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $genre->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $genre->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $genre->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

