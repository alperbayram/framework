<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material[]|\Cake\Collection\CollectionInterface $materials
 */
?>

<h3 class="display-4 pt-3"><?= __('Materials') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('material') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materials as $material): ?>
        <tr align="left">
            <td>
                <a href="/materials/<?=h($material->id)?>">
                    <?= h($material->material) ?>
                </a>
            </td>
            <td><?php if($material->has('parent_material')): ?>
            <a href="/materials/<?=h($material->parent_material->id)?>"><?= h($material->parent_material->material) ?></a>
            <?php endif; ?> </td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $material->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $material->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $material->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

