<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsGenre[]|\Cake\Collection\CollectionInterface $artifactsGenres
 */
?>
<h3 class="display-4 pt-3"><?= __('Artifacts Genres') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('genre_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_genre_uncertain') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsGenres as $artifactsGenre): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactsGenre->id) ?></td> -->
            <td><?= $artifactsGenre->has('artifact') ? $this->Html->link($artifactsGenre->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsGenre->artifact->id]) : '' ?></td>
            <td><?= $artifactsGenre->has('genre') ? $this->Html->link($artifactsGenre->genre->genre, ['controller' => 'Genres', 'action' => 'view', $artifactsGenre->genre->id]) : '' ?></td>
            <td><?= h($artifactsGenre->is_genre_uncertain) ?></td>
            <td>
            <?= $this->Html->link(__('View'), ['action' => 'view',  $artifactsGenre->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactsGenre->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $artifactsGenre->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $artifactsGenre->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

