<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsGenre $artifactsGenre
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsGenre) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Genre') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('genre_id', ['options' => $genres, 'empty' => true]);
                echo $this->Form->control('is_genre_uncertain');
                echo $this->Form->control('comments');
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
