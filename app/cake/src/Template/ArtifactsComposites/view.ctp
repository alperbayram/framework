<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsComposite $artifactsComposite
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Composite') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Composite') ?></th>
                    <td><?= h($artifactsComposite->composite_no) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsComposite->has('artifact') ? $this->Html->link($artifactsComposite->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsComposite->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsComposite->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>
