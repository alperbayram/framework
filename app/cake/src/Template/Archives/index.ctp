<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Archive[]|\Cake\Collection\CollectionInterface $archives
 */
?>


<h3 class="display-4 pt-3"><?= __('Archives') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
<!--             <th scope="col"><?= $this->Paginator->sort('id') ?></th>
 -->            <th scope="col"><?= $this->Paginator->sort('archive') ?></th>
            <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
<!--             <th scope="col"><?= __('Actions') ?></th>
 -->        </tr>
    </thead>
    <tbody>
        <?php foreach ($archives as $archive): ?>
        <tr align="left">
<!--             <td><?= $this->Number->format($archive->id) ?></td>
 -->            <td><a href="/archives/<?=h($archive->id)?>"><?= h($archive->archive) ?></a></td>
            <td><?= $archive->has('provenience') ? $this->Html->link($archive->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $archive->provenience->id]) : '' ?></td>
           <!--  <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $archive->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $archive->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $archive->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $archive->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

