<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDate $artifactsDate
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Date') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsDate->has('artifact') ? $this->Html->link($artifactsDate->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsDate->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Date') ?></th>
                    <td><?= $artifactsDate->has('date') ? $this->Html->link($artifactsDate->date->id, ['controller' => 'Dates', 'action' => 'view', $artifactsDate->date->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsDate->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

</div>



