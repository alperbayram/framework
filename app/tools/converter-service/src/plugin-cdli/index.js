const { plugins } = require('@citation-js/core')
const articles = require('./article')
const artifacts = require('./artifact')
const publications = require('./publication')

plugins.add('@cdli', {
    input: {
        '@cdli/article': {
            parse (article) {
                return articles.convertToTarget(article)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: 'article_type',
                    value (type) {
                        return ['cdlj', 'cdlb', 'cdlp', 'cdln'].includes(type)
                    }
                }
            }
        },
        '@cdli/artifact': {
            parse (artifact) {
                return artifacts.convertToTarget(artifact)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['designation', 'inscriptions']
                }
            }
        },
        '@cdli/publication': {
            parse (publication) {
                return publications.convertToTarget(publication)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['bibtexkey', 'entry_type_id']
                }
            }
        },
        '@cdli/parsed-bibtex': {
            parse ({ _type: type, 'citation-key': label, ...properties }) {
                return { type, label, properties }
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['_type', 'citation-key']
                }
            }
        }
    },
    output: {
        bibliographyandcitations (data, bibliographyEntries = [], citationEntries = [], options = {}) {
            const entries = new Set(data.map(({ id }) => id))
            const missing = new Set()
            for (const ids of [bibliographyEntries, ...citationEntries]) {
                for (let i = ids.length - 1; i >= 0; i--) {
                    if (!entries.has(ids[i])) {
                        missing.add(ids.splice(i, 1)[0])
                    }
                }
            }

            const bibliography = plugins.output.format('bibliography', data, {
                ...options,
                entry: bibliographyEntries
            })
            const citation = citationEntries.map(entries =>
                plugins.output.format('citation', data, {
                    ...options,
                    entry: entries
                })
            )

            return JSON.stringify({
                bibliography,
                citation,
                missing: Array.from(missing)
            })
        }
    }
})
